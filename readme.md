# Peaasi Frontend

A styling static pages for [Peaasi](https://peaasi.ee/)

## Installation

Use the package manager [npm](https://www.npmjs.com/get-npm) to install the packages listed in `package.json`

```bash
npm install
```

## Usage

For running the app

```bash
npm start
```
For building the static files

```bash
npm run build
```

## Manual Usage
first install the `parcel-bundler` globally by running the command

```bash
npm install parcel-bundler
```
you can then run the entry point which is the `index.html` located in `src/pages/`

```bash
parcel src/pages/index.html
```


## Parcel
For more information please read the [Peaasi](https://parceljs.org/getting_started.html) documentation

## License
[MIT](https://choosealicense.com/licenses/mit/)
((window, $) => {
    $('.navigation-toggle').click(function() {
        $(this).find('.burger-menu-container:not(.full-menu-container)').toggleClass('active')

        $('.full-menu-container').toggleClass('stop-scroll')
        $('body').toggleClass('stop-scroll')
    })

    if (navigator.appVersion.indexOf("Win") !== -1)
        $('body').addClass('custom-scrollbar')

    $('.mobile-menu-item .icon').click(function() {
        $(this).parent(`.mobile-menu-item`).toggleClass('mobile-menu-item--active')
    })


    /* SEARCH BAR START */
    $('.navigation-search .icon').click(function() {
        var container = $('.navigation-search-container')

        if (container.hasClass('navigation-search--active')) {
            container.addClass('navigation-search--inactive')
            container.removeClass('navigation-search--active')
        } else {
            container.find('input').val('')
            container.find('button').css({ opacity: 0 })
            container.find('input').css({ opacity: 0 })

            container.removeClass('navigation-search--inactive')
            container.addClass('navigation-search--active')
            container.find('input').focus()

            setTimeout(() => {
                container.find('button').css({ opacity: 1 })
                container.find('input').css({ opacity: 1 })
            }, 1000)
        }
    })

    $(document).mouseup(function(event) {
        var container = $('.navigation-search-container')
        var icon = $('.navigation-search .icon')

        if (!icon.is(event.target) && !container.is(event.target) && !container.has(event.target).length) {
            if (container.hasClass('navigation-search--active')) {
                container.addClass('navigation-search--inactive')
                container.removeClass('navigation-search--active')
            }
        }
    })

    // esc button
    $(document).keyup(function(event) {
        var container = $('.navigation-search-container')
        if (event.keyCode === 27 && container.hasClass('navigation-search--active')) {
            container.addClass('navigation-search--inactive')
            container.removeClass('navigation-search--active')
        }
    })

    /* SEARCH BAR END */
})(window, ($ || jQuery))
